<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index');
Route::post('/user-block/{id}', 'HomeController@userBlockActions')->name('block-actions');
Route::resource('comments', 'CommentController')->only([
    'destroy', 'store'
]);
Route::get('/login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('/login/callback/{provider}', 'Auth\LoginController@handleProviderCallback');


