

## Edit a file


1. Clone the repository
    <br />**git clone https://vladimir_ghukasyan@bitbucket.org/vladimir_ghukasyan/test.git**

2. Switch to the repo folder
    <br />**cd test**

3. Install all the dependencies using composer
    <br />**composer install**

4. Create **.env** file

5. Generate a new application key

   <br />**php artisan key:generate**
  
6. Run the database migrations (Set the database connection in .env before migrating)

   <br />**php artisan migrate**
   <br />**composer dump-autoload**

7. Pass some data in database and create admin

   <br />**php artisan db:seed**
   <br />*admin email is*   **admin@admin.com** ,
   <br />*admin password is*   **admin**

8. Set twitter configs in .env file

   <br />**TWITTER_CLIENT_ID=your_client_id**
   <br />**TWITTER_SECRET=your_secret**
   <br />**TWITTER_CALLBACK_URI=/login/callback/twitter** 
  
9. Start the local development server
     <br />**php artisan serve**

  
  
  
  
  
  
  
  

