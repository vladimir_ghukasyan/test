<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;
use App\User;
use App\Post;

$factory->define(Comment::class, function (Faker $faker) {


    return [
        'text' => $faker->sentence(4),
        'user_id' => User::all()->random()->id,
        'post_id' => Post::all()->random()->id
    ];
});
