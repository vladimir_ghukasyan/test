<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {


    return [
        'title' =>  $faker->unique()->city,
        'body' => $faker->realText(790),
        'stage' =>'production'
    ];
});
