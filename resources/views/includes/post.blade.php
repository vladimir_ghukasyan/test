<article class="article p-3 mb-2"
         style="border: 1px solid grey;border-radius: 5px;box-shadow: 0 0 5px grey;"
         id="article{{$post->id}}">
    <h3 class="text-center">{{$post->title}}</h3>
    <hr>
    <p class="p-2">{{$post->body}}</p>
    <div class="row p-2" style="">
        <div class="btn-group ml-auto">


            <button type="button" class="btn btn-round btn-primary mr-1"
                    onclick='shareTwitter("{{$post->id}}","{{$post->title}}")'
                    title="Share on Twitter"><i class="fa fa-twitter"></i></button>

            <button type="button" class="btn btn-round btn-primary mr-1"
                    data-toggle="collapse" data-target="#comments{{$post->id}}"
                    title="Show Comments"><i class="fa fa-eye"></i></button>



            @can('isActive')
                <button type="button" class="btn btn-round  btn-primary"
                        @if(Auth::check())  onclick='openCommentModal("{{$post->id}}","{{htmlspecialchars_decode($post->title)}}")'
                        @else data-toggle="modal" data-target="#loginModal" @endif

                        title="Write Comment"><i class="fa fa-pencil"></i></button>
            @endcan

        </div>
    </div>
    <div id="comments{{$post->id}}" class="collapse p-1" style="max-height: 400px;overflow-y: auto">
        <h4 class="text-center">Comments</h4>
        @foreach($post->comments as $comment)
            @include('includes.comment')
        @endforeach
    </div>
</article>
