<dl>
    <dt>
        @if(Auth::check() && $comment->user->id === Auth::id()) <strong
            style="color: #1d68a7">You</strong> @else  {{$comment->user->name}}  @endif

        @can('isAdmin')


            @if(Auth::id() !== $comment->user->id)
                <form method="POST"
                      action="{{route('block-actions',$comment->user->id)}}"
                      accept-charset="UTF-8" style="display:inline">
                    @csrf
                    <button type="submit" class="btn"
                            title="@if($comment->user->is_active === 1) Block @else Unlock @endif {{$comment->user->name}}"
                            onclick="return confirm(&quot;Confirm action?&quot;)"><i
                            class="fa @if($comment->user->is_active === 1) fa-lock @else fa-unlock @endif"
                            aria-hidden="true" style="float: right"></i>
                    </button>
                </form>
            @endif
            <form method="POST"
                  action="{{route('comments.destroy',$comment->id)}}"
                  accept-charset="UTF-8" style="display:inline;float: right">
                {{ method_field('DELETE') }}
                @csrf
                <button type="submit" class="btn" style="color: darkred"
                        title="Delete Comment"
                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-remove "
                                                                                 style="float: right"></i>
                </button>
            </form>
        @endcan


    </dt>
    <dd class="text-right" style="color: red"></dd>
    <dd style="color: grey">  {{$comment->text}}</dd>
    <dd style="color: black;text-align: right;font-size: 12px">
        {{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}
    </dd>
</dl>
<hr>

