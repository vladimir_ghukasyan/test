@extends('layouts.app')
@section('style')
    <style>


    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @auth
                @cannot('isActive')
                    <div class="col-md-8">
                        <div class="alert alert-danger alert-dismissible">
                            <strong>Attention!</strong> You are blocked by admin,You can read only.
                        </div>
                    </div>
                @endcannot
            @endauth

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h2 class="text-center">Blogs</h2>
                    </div>

                    <div class="card-body">
                        <div class="infinite-scroll" style="">
                            @foreach($posts as $post)
                                @include('includes.post')
                            @endforeach
                            {{ $posts->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('modals.comment-modal')
        @include('modals.warning-modal')
    </div>
@endsection
@section('script')

    <script type="text/javascript">
        $('ul.pagination').hide();
        $(function () {
            $('.infinite-scroll').jscroll({
                autoTrigger: true,
                debug: true,
                loadingHtml: '<div class="spinner-border text-primary" style="font-size: 30px;"></div>',
                padding: 0,
                nextSelector: '.pagination li.active + li a',
                contentSelector: 'div.infinite-scroll',
                callback: function () {
                    $('ul.pagination').remove();
                }
            });
        });

        function shareTwitter(id,title) {
            {
                window.open("https://twitter.com/share?url=" + escape(window.location.href + '/#article' + id) + "&text=" + title, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
                return false;
            }
        }

        @if(Auth::check())
        @can('isActive')


        function openCommentModal(postId, postTitle) {

            $('#post_id').val(postId);
            $('.title').text(postTitle);
            $('#commentModal').modal()
        }


        $('#commentModal').on('hide.bs.modal', function () {
            $('#post_id').val(null);
            $('#text').val(null);
            $('.title').text(null);
        });

        $('#commentStore').submit(function (e) {
            e.preventDefault();
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                type: 'POST',
                url: '{{route('comments.store')}}',
                data: $(this).serialize(),
                success: function (data) {
                    // window.location.href = '/#article'+data.postId
                    window.location.reload()
                },
                error: function (error) {
                    var resp = error.responseJSON.errors;
                    Object.keys(resp).forEach(function (i) {
                        $('#error').text(resp[i][0])
                    });
                }
            });
        })

        @endcan

        @endif


    </script>
@endsection

