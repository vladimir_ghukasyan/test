<div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right" for="title">Title</label>
    <div class="col-md-6">
        <input type="text" class="form-control  @error('title') is-invalid @enderror" name="title" id="title" value="{{$post->title ?? old('title')}}">
        @error('title')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right" for="stage">Stage</label>
    <div class="col-md-6">
        <select class="form-control  @error('stage') is-invalid @enderror" name="stage" id="stage">
            <option selected disabled>Select one</option>
            <option value="moderation" @if(isset($post) && $post->stage==='moderation') selected @endif>Moderation</option>
            <option value="production" @if(isset($post) && $post->stage==='production') selected @endif>Production</option>
        </select>
        @error('stage')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right" for="body">Body</label>
    <div class="col-md-6">
        <textarea class="form-control  @error('body') is-invalid @enderror" id="body" name="body">@if(isset($post) ){{$post->body}}@else{{old('body')}}@endif</textarea>
        @error('body')
        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
        @enderror
    </div>
</div>

