@extends('admin.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> NEW POST</div>

                    <div class="card-body">
                        <div class="row p-2" >
                            <a href="{{route('posts.index')}}" class="btn btn-primary ml-auto" >Back </a>
                        </div>
                        <form method="POST" action="{{route('posts.store')}}">
                            @csrf
                            @include('admin.posts.form')
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
