@extends('admin.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Posts</div>

                    <div class="card-body">
                        <div class="row p-2" >
                            <a href="{{route('posts.create')}}" class="btn btn-primary  ml-auto" style="float: right!important;">Add </a>
                        </div>

                        <div class="row p-2">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>TITLE</th>
                                        <th>STAGE</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($posts as $post)
                                        <tr>
                                            <td>{{$post->id}}</td>
                                            <td>{{$post->title}}</td>
                                            <td>{{$post->stage}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a style="margin-right: 4px"
                                                       href="{{ route('posts.edit', $post->id) }}"
                                                       title="Edit Post">
                                                        <button class="btn btn-success">Edit
                                                        </button>
                                                    </a>

                                                    <form method="POST"
                                                          action="{{ route('posts.destroy',  $post->id) }}"
                                                          accept-charset="UTF-8" style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class="btn btn-danger"
                                                                title="Delete Post"
                                                                onclick="return confirm(&quot;Confirm delete?&quot;)">Del
                                                        </button>
                                                    </form>
                                                </div>

                                            </td>
                                        </tr>
                                    @endforeach



                                    </tbody>
                                </table>
                                {{$posts->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
