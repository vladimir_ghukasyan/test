<div class="modal" id="commentModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Comment for <span class="title" style="color: #3f9ae5"></span></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="POST" id="commentStore">
                    <div class="form-group row  p-2">
                        <label for="text"><strong class="p-2">Text</strong></label>
                        <textarea class="form-control" id="text" rows="5" name="text" required
                                  maxlength="800"></textarea>
                    </div>
                    <input type="hidden" name="post_id" id="post_id">

                    <span class="invalid-feedback" role="alert" style="display: block">
                                        <strong id="error"></strong>
                            </span>

                    <div class="form-group row  p-2">
                        <div class="btn-group ml-auto">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
