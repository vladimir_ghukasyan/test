<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'user_id', 'post_id', 'text',
    ];

    protected $dates = [
        'created_at',
    ];

    protected $with = ['user','post'];


    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function post(){
        return $this->hasOne(Post::class,'id','post_id');
    }
}
