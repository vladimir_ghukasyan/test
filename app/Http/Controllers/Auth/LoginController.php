<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();

    }
    public function handleProviderCallback($provider)
    {

        try {
            $user = Socialite::driver($provider)->user();

        } catch (\Exception $e) {

            return redirect('/login');
        }



        $user = User::updateOrCreate(
            [
                'email' =>$user->name.'@twitter.com'
            ],
            [
                'name'=>$user->name,
            ]

        );

        Auth::login($user, true);
        return redirect()->intended('/home');

    }


    protected function authenticated($request,$user){

        if($user->is_admin === 1){
            return redirect()->intended('/admin/posts'); //redirect to admin panel
        }
        return redirect()->intended('/home'); //redirect to standard user homepage
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
