<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('can:isAdmin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $posts = Post::orderBy('id','desc')->paginate(20);
        return view('admin.posts.index', compact('posts'));
    }

    public function create()
    {
        return view('admin.posts.create');
    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.posts.edit', compact('post'));
    }

    public function destroy($id)
    {
        Post::destroy($id);
        flash('Successfully deleted post');
        return redirect()->back();

    }

    public function update(PostRequest $request,$id)
    {
        $post = Post::findOrFail($id);
        $post->update($request->all());
        flash('Successfully updated post');

        return redirect()->route('posts.index');
    }

    public function store(PostRequest $request)
    {

        Post::create($request->all());
        flash('Successfully created new post');

        return redirect()->route('posts.index');

    }
}
