<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','can:isActive']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(CommentRequest $request)
    {

        $post = Post::whereId($request->post_id)->first();

        if(!$post){
            return response()->json(['errors'=>['post'=>['Post not found']]],421);
        }

        $request->request->add(['user_id'=>Auth::id()]);
        Comment::create($request->all());
        return response()->json(['postId'=>$request->post_id],200);
    }

    public function destroy($id)
    {
        $this->middleware('can:isAdmin');

        Comment::destroy($id);
        return redirect()->back();
    }

}
