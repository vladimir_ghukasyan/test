<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $posts = Post::where('stage', 'production')->with(['comments' => function ($q) {
            return $q->orderBy('created_at', 'desc');
        }])->orderBy('created_at', 'desc')->paginate(5);


        return view('home', compact('posts'));
    }

    public function userBlockActions($id)
    {
        $this->middleware('can:isAdmin');


        $user = User::findOrFail($id);

        if ($user->is_active === 1) {
            $user->update(['is_active' => 0]);
        } else {
            $user->update(['is_active' => 1]);
        }

        return redirect()->back();

    }

}
