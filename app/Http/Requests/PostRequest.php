<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        $rules = [
            'title' => 'required|regex:/^[\pL\s\-]+$/u|string|max:20|unique:posts,title',
            'body' => 'required|string|max:800',
            'stage' => 'required',
        ];


        if (Request::method() === 'PUT') {
            $rules['title'] = $rules['title'] . ',' . $this->route('post');
        }

        return $rules;
    }
}
